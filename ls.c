#include "ls.h"

struct file_ll * make_file_node (char * file_name)
{
    struct file_ll * new_node;

    new_node = malloc(sizeof(struct file_ll));
    new_node->next = NULL;
    new_node->children = NULL;
    new_node->name = malloc (strlen(file_name) + 1);
    memset(new_node->name, 0, strlen(file_name) + 1);
    strcpy(new_node->name, file_name);
    return new_node;
}

struct file_ll * insert_file_node (struct file_ll * head, struct file_ll * node)
{
    struct file_ll * cur_node;

    cur_node = head;
    if (head == NULL) {
        return node;
    }
    else if (lsstrcmp(cur_node->name, node->name) > 0) {
        node->next = head;
        return node;
    }
    for (cur_node = head; cur_node->next != NULL; cur_node = cur_node->next) {
        if (lsstrcmp(cur_node->name, node->name) < 0 && lsstrcmp (cur_node->next->name, node->name) > 0) {
            break;
        }
    }
    node->next = cur_node->next;
    cur_node->next = node;
    return head;
}

int lsstrcmp (char * a, char * b)
{
    char * first;
    char * second;

    if (strcmp(".", a) == 0) {
        return -1;
    }
    if (strcmp(".", b) == 0) {
        return 1;
    }

    if (a[0] == '.') {
        first = a + 1;
    }
    else {
        first = a;
    }
    if (b[0] == '.') {
        second = b + 1;
    }
    else {
        second = b;
    }
    return strcmp(first, second);
}

void delete_file_list (struct file_ll * head)
{
    struct file_ll * cur_node;
    struct file_ll * next_node;

    cur_node = head;
    while (cur_node != NULL) {
        next_node = cur_node->next;
        free(cur_node->name);
        if (cur_node->children != NULL) {
            delete_file_list(cur_node->children);
        }
        free(cur_node);
        cur_node = next_node;
    }
}

struct file_ll * populate_files (char * dir_name, int recurse, int show_hidden)
{
    DIR * dir;
    char * recurse_name;
    struct dirent * file;
    struct stat statbuf;

    struct file_ll * head;
    struct file_ll * new_node;

    int dir_name_len;
    int file_name_len;

    head = NULL;

    errno = 0;
    dir = opendir(dir_name);
    if (errno) {
        perror (NULL);
        exit (1);
    }
    file = readdir(dir);
    while (file != NULL) {
        if (!show_hidden && file->d_name[0] == '.') {
            file = readdir(dir);
            continue;
        }
        new_node = make_file_node(file->d_name);
        head = insert_file_node(head, new_node);
        if (recurse && strcmp(file->d_name, ".") && strcmp(file->d_name, "..")) {
            dir_name_len = strlen(dir_name);
            file_name_len = strlen(file->d_name);

            /* set up recurse_name value */
            recurse_name = malloc(dir_name_len + file_name_len + 2); /* +2 for the '/' between the two and the null byte at the end */
            memset(recurse_name, 0, dir_name_len + file_name_len + 2);
            strcpy(recurse_name, dir_name);
            *(recurse_name + dir_name_len) = '/';
            strcpy(recurse_name + dir_name_len + 1, file->d_name);

            //printf("%s\n", recurse_name);
            errno = 0;
            if (lstat(recurse_name, &statbuf) != -1) {
                if ((statbuf.st_mode & S_IFMT) == S_IFDIR) {
                    new_node->children = populate_files(recurse_name, recurse, show_hidden);
                }
            }
            else {
                printf ("ERROR: %x\n", errno);
            }
            free(recurse_name);
        }
        file = readdir(dir);
    }
    closedir(dir);
    return head;
}

void print_files (struct file_ll * head, int recurse_level)
{
    struct file_ll * cur_node;
    int i;

    for (cur_node = head; cur_node != NULL; cur_node = cur_node->next) {
        for (i = 0; i < recurse_level; i++) {
            printf("\t");
        }
        printf("%s\n", cur_node->name);
        if (cur_node->children != NULL) {
            print_files(cur_node->children, recurse_level + 1);
        }
    }
}
