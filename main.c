#include "ls.h"
#include <argp.h>

const char * argp_program_version = "HH ls 1.0";

static char doc[] = "self programmed ls for practice";
stat char args_doc[] = "filename";

struct argp_option options[] = {
    { "all", 'a', 0, 0, "Show hidden files" },
    { "recursive", 'r', 0, 0, "Recurse into directories" },
    {0}
};

struct arguments
{
    char * args[1];
    int all;
    int recurse;
};

static error_t parse_opt (int key, char *arg, struct argp_state *state)
{
    struct arguments *arguments = state->input;
    switch (key) {
        case 'a':
            arguments->all = 1;
            break;
        case 'r':
            arguments->recurse = 1;
            break;
        case ARGP_KEY_ARG:
            if (state->arg_num > 2)
    }
}

int main (int argc, char * argv[])
{
    char * dir_name;
    struct file_ll * head;

    if (argc == 1) {
        dir_name = DEFAULT_PATH;
    }
    else {
        dir_name = argv[1];
    }
    head = populate_files(dir_name, RECURSIVE, 0);
    print_files(head, 0);
    delete_file_list(head);
}
