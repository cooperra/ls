#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>

#define DEFAULT_PATH "."
#define RECURSIVE 1

struct file_ll {
    char * name;
    struct file_ll * next;
    struct file_ll * children;
};

struct file_ll * make_file_node (char * file_name);
struct file_ll * insert_file_node (struct file_ll * head, struct file_ll * node);

int lsstrcmp(char * first, char * second);

struct file_ll * populate_files (char * dir_name, int recurse, int show_hidden);
void delete_file_list (struct file_ll * head);
void print_files (struct file_ll * head, int recurse_level);
